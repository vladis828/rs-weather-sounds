import summerPic from '../public/pics/summer-bg.jpg';
import rainPic from '../public/pics/rainy-bg.jpg';
import winterPic from '../public/pics/winter-bg.jpg';
import pausePic from '../public/icons/pause.svg';

const pictures: { [key: string]: Object } = {
  summer: summerPic,
  rain: rainPic,
  winter: winterPic,
  pause: pausePic,
};

export default pictures;
