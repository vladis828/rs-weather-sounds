import './style.scss';
import addVolumeSlider from './volumeSlider';
import addSummerCard from './summerCard';
import addWinterCard from './winterCard';
import addRainCard from './rainCard';

const app = document.getElementById('app')!;

const header = document.createElement('h1');
header.textContent = 'Weather sounds';
app.appendChild(header);

const cardsContainer = document.createElement('div');
cardsContainer.classList.add('cards-container');
app.appendChild(cardsContainer);

addVolumeSlider(app);

addSummerCard(cardsContainer);
addRainCard(cardsContainer);
addWinterCard(cardsContainer);

export default app;
