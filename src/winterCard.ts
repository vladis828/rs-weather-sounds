import cardEventHandler from './cardEventHandler';

const addWinterCard = (parentElement: HTMLElement) => {
  const cardWinter = document.createElement('div');
  cardWinter.classList.add('card');
  cardWinter.id = 'card-winter';
  cardWinter.addEventListener('click', () => {
    cardEventHandler('winter');
  });
  const winterIconContainer = document.createElement('div');
  winterIconContainer.classList.add('card-icon');
  winterIconContainer.id = 'card-winter-icon';
  cardWinter.appendChild(winterIconContainer);
  parentElement.appendChild(cardWinter);
};

export default addWinterCard;
