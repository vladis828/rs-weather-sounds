import cardEventHandler from './cardEventHandler';

const addSummerCard = (parentElement: HTMLElement) => {
  const cardSummer = document.createElement('div');

  cardSummer.classList.add('card');
  cardSummer.id = 'card-summer';
  cardSummer.addEventListener('click', () => {
    cardEventHandler('summer');
  });
  const summerIconContainer = document.createElement('div');
  summerIconContainer.classList.add('card-icon');
  summerIconContainer.id = 'card-summer-icon';
  cardSummer.appendChild(summerIconContainer);
  parentElement.appendChild(cardSummer);
};

export default addSummerCard;
