import cardEventHandler from './cardEventHandler';

const addRainCard = (parentElement: HTMLElement) => {
  const cardRain = document.createElement('div');
  cardRain.classList.add('card');
  cardRain.id = 'card-rain';
  cardRain.addEventListener('click', () => {
    cardEventHandler('rain');
  });
  const rainIconContainer = document.createElement('div');
  rainIconContainer.classList.add('card-icon');
  rainIconContainer.id = 'card-rain-icon';
  cardRain.appendChild(rainIconContainer);
  parentElement.appendChild(cardRain);
};

export default addRainCard;
